__version__ = "0.0.0"

import argparse
from datetime import datetime
from pathlib import Path
import shlex
import shutil
import subprocess
import sys

from jinja2 import Environment, FileSystemLoader, select_autoescape  # type: ignore
import toml

import webservice


def get_packaging_statistics(deps) -> dict[str : tuple[int, float, str]]:
    """Get packaging statistics of dependencies.

    Calculate packaging statistics of dependencies in numbers as well as in
    percentage and then return a dictionary containing these information.
    """

    satisfied = 0
    invalid = 0
    not_sure = 0
    unsatisfied = 0
    wnpp = 0
    no_wnpp = 0
    total = len(deps)

    for dep, attribute in deps.items():
        if attribute["status"] == "Packed":
            if attribute["satisfied"] == "Yes":
                satisfied += 1
            elif "Not sure" in attribute["satisfied"]:
                not_sure += 1
            elif "Invalid version format" in attribute["satisfied"]:
                invalid += 1
            else:
                unsatisfied += 1
        else:
            if "Not found" in attribute["wnpp"]:
                no_wnpp += 1
            else:
                wnpp += 1

    satisfied_percentage = (satisfied / total) * 100
    invalid_percentage = (invalid / total) * 100
    not_sure_percentage = (not_sure / total) * 100
    unsatisfied_percentage = (unsatisfied / total) * 100
    wnpp_percentage = (wnpp / total) * 100
    no_wnpp_percentage = (no_wnpp / total) * 100

    status = {
        "satisfied": [satisfied, satisfied_percentage, "satisfied"],
        "invalid": [invalid, invalid_percentage, "invalid"],
        "not-sure": [not_sure, not_sure_percentage, "unknown"],
        "unsatisfied": [unsatisfied, unsatisfied_percentage, "unsatisfied"],
        "wnpp": [wnpp, wnpp_percentage, "wnpp"],
        "no-wnpp": [no_wnpp, no_wnpp_percentage, "not packed"],
    }

    return status


def generate_status_page(template, pkgs, envs) -> None:
    color_map = {
        "Debian: Invalid version format": "table-primary",  # Sky Blue
        "Upstream: Invalid version format": "table-primary",  # Sky Blue
        "Not sure (unstable version)": "table-warning",  # Yellow
        "Not sure": "table-warning",  # Yellow
        "Not sure (Commit revision isn't matching.)": "table-warning",  # Yellow
        "Not sure (Upstream version is a pseudo version, but available version is a release version.)": "table-warning",  # Yellow
        "Not sure (Upstream version is a release version, but available version is a pseudo version.)": "table-warning",  # Yellow
        "No": "table-danger",  # Red
        "Yes": "table-success",  # Green
    }

    for package, attributes in pkgs.items():
        revision = attributes["revision"]
        print(f"""Generating status page of "{package} ({revision})" package...""")
        status = f"public/data/toml/{package}-{revision}_packaging_status.toml"
        with open(status) as fp:
            deps = toml.load(fp)

        statistics = get_packaging_statistics(deps["require"])
        output = template.render(
            envs=envs,
            pkgs=pkgs,
            color_map=color_map,
            package=package,
            revision=revision,
            deps=deps,
            count=statistics,
        )

        with open(f"public/pkg/{package}-{revision}.html", "w") as fp:
            fp.write(output)


def generate_index_page(template, pkgs, envs) -> None:
    output = template.render(pkgs=pkgs, envs=envs)
    with open("public/index.html", "w") as fp:
        fp.write(output)


def packaging_status(
    package: str, url: str, revision: str, exception_file: str, output_file: str
) -> None:
    print(f"""Processing packaging status of "{package} ({revision})" package...""")
    command = shlex.split(
        " ".join(
            [
                "godeb",
                url,
                revision,
                f"--exception {exception_file}",
                f"--output {output_file}",
            ]
        )
    )
    subprocess.run(command, stdout=subprocess.PIPE)


def prepare_dirs(root_dir: str) -> None:
    """Prepare output directory."""

    # Make a clean copy of "static/" directory into the output directory.
    shutil.rmtree("public/static", ignore_errors=True)
    shutil.copytree(f"{root_dir}/static", "public/static", dirs_exist_ok=True)

    # Clean "pkg/" subdirectory, then recreate it.
    shutil.rmtree("public/pkg", ignore_errors=True)
    dirs = Path("public/pkg")
    dirs.mkdir(parents=True, exist_ok=True)


def environment_vars() -> dict[str:str]:
    envs = {}

    # Get all environment variables from "config" file.
    config = Path("config.toml")
    if config.is_file():
        with open("config.toml") as fp:
            envs = toml.load(fp)
    else:
        print("No such file or directory: 'config.toml'", file=sys.stderr)

    # If "base_url" contains trailing "/", remove it. Otherwise some hyperlinks
    # break in HTML pages.
    if "base_url" in envs:
        envs["base_url"] = envs["base_url"].rstrip("/")

    date = datetime.utcnow()
    date = date.strftime("%a, %b %d, %Y (%I:%M %p UTC)")
    envs["date"] = date

    return envs


def create_arg_parser() -> argparse.ArgumentParser:
    """Return the command-line interface of the program."""

    parser = argparse.ArgumentParser(
        prog="webservice",
        description="Build Godeb web interface, a Go mod dependency checker.",
        add_help=False,
    )

    build = parser.add_mutually_exclusive_group()
    build.add_argument(
        "--fast-build",
        action="store_true",
        help="""Build the website without generating packaging status. It'll use cached status available in "public/data/" directory, generated from previous complete build process. (Default built type.)""",
    )
    build.add_argument(
        "--full-build",
        action="store_true",
        help="""Build the complete website including generating packaging status of all packages listed in "packages.toml" file. This may take a while to complete the process build.""",
    )

    parser.add_argument(
        "-e",
        "--exception",
        metavar="FILE",
        help="Load exception list TOML file. (Default: exceptions.toml)",
    )
    parser.add_argument(
        "--gitlab-pages",
        action="store_true",
        help="Build webpages for publishing the website using Gitlab pages. Despites its name, it could also be used for Github, Sourcehut pages and many more.",
    )
    parser.add_argument(
        "-p",
        "--packages",
        metavar="FILE",
        help="Load package list TOML file. (Default: packages.toml)",
    )

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=f"%(prog)s {__version__}",
        help="Show program's version number and exit.",
    )
    parser.add_argument(
        "-h", "--help", action="help", help="Show this help message and exit."
    )

    return parser


def main() -> int:
    arg_parser = create_arg_parser()
    args = arg_parser.parse_args()

    # Get environment variables.
    env_vars = environment_vars()

    # If "--gitlab-pages" option not set, set "base_url" to an empty string to
    # make local deployment work.
    if not args.gitlab_pages:
        env_vars["base_url"] = ""

    # Get the location of the package.
    root_dir = (webservice.__path__)[0]

    # Remove the output directory to get a clean build.
    if args.full_build:
        shutil.rmtree("public", ignore_errors=True)

    prepare_dirs(root_dir)

    env = Environment(
        loader=FileSystemLoader(f"{root_dir}/templates"), autoescape=select_autoescape()
    )

    package_list = "packages.toml"
    if args.packages:
        package_list = args.packages

    with open(package_list) as fp:
        pkgs = toml.load(fp)

    available_pkgs = {}

    for package, attributes in pkgs.items():
        url = attributes["url"]
        revision = attributes["revision"]
        exception_file = "exceptions.toml"
        output_file = f"public/data/toml/{package}-{revision}_packaging_status.toml"

        if args.exception:
            exception_file = args.exception

        if args.full_build:
            packaging_status(package, url, revision, exception_file, output_file)

        status = Path(f"{output_file}")
        if status.is_file():
            available_pkgs[package] = attributes
        else:
            print(
                f"""Packaging status data isn't generated for "{package} ({revision})" package. Try "--full-build" option.""",
                file=sys.stderr,
            )

    generate_index_page(env.get_template("index.html"), available_pkgs, env_vars)
    generate_status_page(env.get_template("status.html"), available_pkgs, env_vars)

    return 0


if __name__ == "__main__":
    sys.exit(main())
